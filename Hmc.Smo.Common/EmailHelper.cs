﻿using System;

namespace Hmc.Smo.Common
{
    public static class EmailHelper
    {
        public static void GetMailContent(string hd, string bodyTemplate, string body, string dear, string docNo, string url,
            string soldTo, string saleConfrimNo, out string o_hd, out string o_body, string actor, string lineItems,
            string customerName, string reason, string headerTh, string headerEn, string footerTh, string footerEn,
            string saleOrderNo)
        {
            DateTime d = DateTime.Today;
            string msg = string.Empty;
            saleOrderNo = string.IsNullOrEmpty(saleOrderNo.Trim()) ? "-" : saleOrderNo;

            DateTime rs;

            if (d.DayOfWeek == DayOfWeek.Sunday)
            {
                rs = d;
            }
            else
            {
                int offset = d.DayOfWeek - DayOfWeek.Monday;

                DateTime lastMonday = d.AddDays(-offset);
                DateTime nextSunday = lastMonday.AddDays(6);

                rs = nextSunday;
            }

            o_hd = hd.Replace("<DocNo>", docNo);
            o_hd = o_hd.Replace("<url>", url);
            o_hd = o_hd.Replace("<SoldTo>", soldTo);
            o_hd = o_hd.Replace("<SaleConfirmNo>", saleConfrimNo);
            o_hd = o_hd.Replace("<Actor>", actor);
            o_hd = o_hd.Replace("<CustomerName>", customerName);
            o_hd = o_hd.Replace("<SaleOrderNo>", saleOrderNo);

            string dueTH = rs.ToString("dd/MM/yyyy") + " เวลา 23:59";
            string dueEN = rs.ToString("dd/MM/yyyy") + " at 23:59";

            o_body = string.Format(bodyTemplate, dear, body);
            o_body = o_body.Replace("<DocNo>", docNo);
            o_body = o_body.Replace("<url>", url);
            o_body = o_body.Replace("<SoldTo>", soldTo);
            o_body = o_body.Replace("<SaleConfirmNo>", saleConfrimNo);
            o_body = o_body.Replace("<Actor>", actor);
            o_body = o_body.Replace("<LineItems>", lineItems);
            o_body = o_body.Replace("<Reason>", reason);
            o_body = o_body.Replace("<DueDateTH>", dueTH);
            o_body = o_body.Replace("<DueDateEN>", dueEN);
            o_body = o_body.Replace("<HeaderTH>", headerTh);
            o_body = o_body.Replace("<HeaderEN>", headerEn);
            o_body = o_body.Replace("<FooterTH>", footerTh);
            o_body = o_body.Replace("<FooterEN>", footerEn);
            o_body = o_body.Replace("<SaleOrderNo>", saleOrderNo);
        }

        public static void GetMailContentV2(string hd, string bodyTemplate, string body, string dear, string docNo, string url,
            string soldTo, string saleConfrimNo, out string o_hd, out string o_body, string actor, string lineItems,
            string customerName, string reason, string headerTh, string headerEn, string footerTh, string footerEn,
            string poNo, string saloOrder, string shipToName, string address, DateTime? deliveryDate, string shipmentNo, string invoiceNo)
        {
            DateTime d = DateTime.Today;
            string msg = string.Empty;
            string dateFormat = "dd/MM/yyyy";

            DateTime rs;

            if (d.DayOfWeek == DayOfWeek.Sunday)
            {
                rs = d;
            }
            else
            {
                int offset = d.DayOfWeek - DayOfWeek.Monday;

                DateTime lastMonday = d.AddDays(-offset);
                DateTime nextSunday = lastMonday.AddDays(6);

                rs = nextSunday;

            }

            o_hd = hd.Replace("<DocNo>", docNo);
            o_hd = o_hd.Replace("<url>", url);
            o_hd = o_hd.Replace("<SoldTo>", soldTo);
            o_hd = o_hd.Replace("<SaleConfirmNo>", saleConfrimNo);
            o_hd = o_hd.Replace("<Actor>", actor);
            o_hd = o_hd.Replace("<CustomerName>", customerName);
            o_hd = o_hd.Replace("<PoNo>", poNo);
            o_hd = o_hd.Replace("<InvoiceNo>", invoiceNo);

            string dueTH = rs.ToString("dd/MM/yyyy") + " เวลา 23:59";
            string dueEN = rs.ToString("dd/MM/yyyy") + " at 23:59";

            o_body = string.Format(bodyTemplate, dear, body);
            o_body = o_body.Replace("<DocNo>", docNo);
            o_body = o_body.Replace("<url>", url);
            o_body = o_body.Replace("<SoldTo>", soldTo);
            o_body = o_body.Replace("<SaleConfirmNo>", saleConfrimNo);
            o_body = o_body.Replace("<Actor>", actor);
            o_body = o_body.Replace("<LineItems>", lineItems);
            o_body = o_body.Replace("<Reason>", reason);
            o_body = o_body.Replace("<DueDateTH>", dueTH);
            o_body = o_body.Replace("<DueDateEN>", dueEN);
            o_body = o_body.Replace("<HeaderTH>", headerTh);
            o_body = o_body.Replace("<HeaderEN>", headerEn);
            o_body = o_body.Replace("<FooterTH>", footerTh);
            o_body = o_body.Replace("<FooterEN>", footerEn);

            string deliveryDateStr = deliveryDate.HasValue ? deliveryDate.Value.ToString(dateFormat) : "-";

            o_body = o_body.Replace("<PoNo>", poNo);
            o_body = o_body.Replace("<SaloOrder>", saloOrder);
            o_body = o_body.Replace("<CustomerName>", customerName);
            o_body = o_body.Replace("<ShipToName>", shipToName);
            o_body = o_body.Replace("<Address>", address);
            o_body = o_body.Replace("<DeliveryDate>", deliveryDateStr);
            o_body = o_body.Replace("<ShipmentNo>", shipmentNo);
        }
    }
}
