﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Hmc.Smo.Wcf
{
    public enum Align
    {
        Left, Center, Right
    }

    public class HtmlTableRowFormat
    {
        public int Col { get; set; }
        public Align Align { get; set; }
        public string HeaderText { get; set; }
        public string AlignText
        {
            get
            {
                return Align.ToString();
            }
        }
        public string ColText
        {
            get
            {
                return "{" + Col.ToString() + "}";
            }
        }

        public HtmlTableRowFormat(int col, Align align, string headerText)
        {
            Col = col;
            Align = align;
            HeaderText = headerText;
        }
    }

    public class Invoice
    {
        public string PoNo { get; set; }
        public string QuotationNo { get; set; }
        public string SaleOrder { get; set; }
        public string SoldTo { get; set; }
        public string ShipTo { get; set; }
        public string Address { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string ShipmentNo { get; set; }
        public string DeliveryOrder { get; set; }
        public string ProductName { get; set; }
        public decimal Quantity { get; set; }
        public string InvoiceNo { get; set; }
        public string SoldToName { get; set; }
        public string PackSizeName { get; set; }
        public string ShipToName { get; set; }
        public string IsCancel { get; set; }
    }

    public enum ShipmentStatus
    {
        All, Complete, NotComplete
    }
    public class SearchShipmentCriteria
    {
        private string _quotationNo;

        public string QuotationNo
        {
            get { return E2N(_quotationNo); }
            set { _quotationNo = value; }
        }
        private string _soldTo;

        public string SoldTo
        {
            get { return E2N(_soldTo); }
            set { _soldTo = value; }
        }
        private string _shipTo;

        public string ShipTo
        {
            get { return E2N(_shipTo); }
            set { _shipTo = value; }
        }
        private string _productName;

        public string ProductName
        {
            get { return E2N(_productName); }
            set { _productName = value; }
        }
        private string _custRef;

        public string CustRef
        {
            get { return E2N(_custRef); }
            set { _custRef = value; }
        }
        private string _shipmentNo;

        public string ShipmentNo
        {
            get { return E2N(_shipmentNo); }
            set { _shipmentNo = value; }
        }
        private string _docNo;

        public string DocNo
        {
            get { return E2N(_docNo); }
            set { _docNo = value; }
        }
        private string _saleOrder;

        public string SaleOrder
        {
            get { return E2N(_saleOrder); }
            set { _saleOrder = value; }
        }
        private string _deliveryNo;

        public string DeliveryNo
        {
            get { return E2N(_deliveryNo); }
            set { _deliveryNo = value; }
        }
        private string _invoiceNo;

        public string InvoiceNo
        {
            get { return E2N(_invoiceNo); }
            set { _invoiceNo = value; }
        }

        private string _sMONo;
        public string SMONo
        {
            get { return E2N(_sMONo); }
            set { _sMONo = value; }
        }
        private string _InvoiceType;
        public string InvoiceType
        {
            get { return E2N(_InvoiceType); }
            set { _InvoiceType = value; }
        }

        public DateTime? DeliveryDate { get; set; }
        public DateTime? DocDate { get; set; }
        public ShipmentStatus ShipmentStatus { get; set; }
        //public string QuotationNo { get; set; }
        //public string SoldTo { get; set; }
        //public string ShipTo { get; set; }
        //public string ProductName { get; set; }

        //public string CustRef { get; set; }

        //public string ShipmentNo { get; set; }

        //public string DocNo { get; set; }

        //public string SaleOrder { get; set; }
        //public string DeliveryNo { get; set; }
        //public string InvoiceNo { get; set; }

        private string E2N(string str)
        {
            var newStr = string.IsNullOrEmpty(str) ? null : str;
            return newStr;
        }
    }

    public class SearchAdvanceSalesCriteria
    {
        private string _soldTo;

        public string SoldTo
        {
            get { return E2N(_soldTo); }
            set { _soldTo = value; }
        }
        private string _shipTo;

        public string ShipTo
        {
            get { return E2N(_shipTo); }
            set { _shipTo = value; }
        }
        private string _saleConfirmation;

        public string SaleConfirmation
        {
            get { return E2N(_saleConfirmation); }
            set { _saleConfirmation = value; }
        }
        private string _saleOrder;

        public string SaleOrder
        {
            get { return E2N(_saleOrder); }
            set { _saleOrder = value; }
        }
        public DateTime? InvoiceDate { get; set; }
        private string _invoiceNo;

        public string InvoiceNo
        {
            get { return E2N(_invoiceNo); }
            set { _invoiceNo = value; }
        }
        private string _deliveryOrder;

        public string DeliveryOrder
        {
            get { return E2N(_deliveryOrder); }
            set { _deliveryOrder = value; }
        }
        public DateTime? DeliveryDate { get; set; }



        private string _shipmentNo;

        public string ShipmentNo
        {
            get { return E2N(_shipmentNo); }
            set { _shipmentNo = value; }
        }
        public DateTime? ShipmentDate { get; set; }





        private string _productGrade;

        public string ProductGrade
        {
            get { return E2N(_productGrade); }
            set { _productGrade = value; }
        }

        private string _po;

        public string PO
        {
            get { return E2N(_po); }
            set { _po = value; }
        }

        private string _documentNo;

        public string DocumentNo
        {
            get { return E2N(_documentNo); }
            set { _documentNo = value; }
        }

        
       


        private string E2N(string str)
        {
            var newStr = string.IsNullOrEmpty(str) ? null : str;
            return newStr;
        }
    }

    public class WebConfig
    {
        public int WEB_QTY_MIN { get; set; } //= 25;
        public int WEB_ITEM_MAX { get; set; } // = 10;
        public int WEB_QTY_MAX { get; set; } // = 2000000;
        public string WEB_APP_TYPE { get; set; } //= "PRD";
        public string WEB_UP_EXTS { get; set; } //= "xls, xlsx, doc, docx, txt, pdf, jpg, jpeg, png";
        public int WEB_UP_MAX_FILE { get; set; } //= 5;
        public int WEB_UP_MAX_SIZE { get; set; } //= 1;
    }

    public class SaleConfirmStatusSMO13
    {
        public string SaleConfirmNo { get; set; }
        public string OrderNo { get; set; }
        public string Status { get; set; }
        public string SaleOrderNo { get; set; }
    }

    public partial class Order
    {
        public string SaleOrder { get; set; }
    }
    public partial class User
    {
        [DataMember]
        public bool IsAdmin { get; set; }
    }
}