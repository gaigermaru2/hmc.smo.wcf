﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Objects;

namespace Hmc.Smo.Wcf
{
    public class SmoDA
    {

        #region OrderTemplates
        public OrderTemplate CreateOrderTemplate(OrderTemplate orderTemplate)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.OrderTemplates.AddObject(orderTemplate);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, orderTemplate);
            }
            return orderTemplate;
        }
        public OrderTemplate UpdateOrderTemplate(OrderTemplate orderTemplate)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.OrderTemplates.Attach(orderTemplate);
                ctx.ObjectStateManager.GetObjectStateEntry(orderTemplate).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, orderTemplate);
            }
            return orderTemplate;
        }
        public OrderTemplate DeleteOrderTemplate(OrderTemplate orderTemplate)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.OrderTemplates.Attach(orderTemplate);
                ctx.OrderTemplates.DeleteObject(orderTemplate);
                ctx.SaveChanges();
            }
            return orderTemplate;
        }
        public OrderTemplate SelectOrderTemplateByPK(string iD, string customerNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpOrderTemplateSelectByPK(iD, customerNo).FirstOrDefault();
            }
        }
        public List<OrderTemplateForList> SelectOrderTemplatesForList(string customerNo, bool? allowDownload)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpOrderTemplatesSelectForList(customerNo, allowDownload).ToList();
            }
        }

        #endregion

        #region ReportShipmentBalances
        public ReportShipmentBalance CreateReportShipmentBalance(ReportShipmentBalance reportShipmentBalance)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.ReportShipmentBalances.AddObject(reportShipmentBalance);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, reportShipmentBalance);
            }
            return reportShipmentBalance;
        }
        public ReportShipmentBalance UpdateReportShipmentBalance(ReportShipmentBalance reportShipmentBalance)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.ReportShipmentBalances.Attach(reportShipmentBalance);
                ctx.ObjectStateManager.GetObjectStateEntry(reportShipmentBalance).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, reportShipmentBalance);
            }
            return reportShipmentBalance;
        }
        public ReportShipmentBalance DeleteReportShipmentBalance(ReportShipmentBalance reportShipmentBalance)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.ReportShipmentBalances.Attach(reportShipmentBalance);
                ctx.ReportShipmentBalances.DeleteObject(reportShipmentBalance);
                ctx.SaveChanges();
            }
            return reportShipmentBalance;
        }
        public ReportShipmentBalance SelectReportShipmentBalanceByPK(string reportShipmentBalanceID)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpReportShipmentBalanceSelectByPK(reportShipmentBalanceID).FirstOrDefault();
            }
        }
        public List<ReportShipmentBalance> SelectReportShipmentBalancesForList(SearchShipmentCriteria s)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                var rs = ctx.SpReportShipmentBalancesSelectForList(s.DocDate, s.QuotationNo, null, s.SoldTo, s.ShipTo, null, null, null, null, null, null, null, s.CustRef, s.ProductName, null, null, s.DocNo).ToList();

                if (s.ShipmentStatus == ShipmentStatus.Complete)
                {
                    //rs = rs.Where(o => o.QtyBackOrder.HasValue && o.QtyBackOrder.Value == 0).ToList();

                    rs = rs.Where(o => o.QtySaleConfirm.HasValue&& o.QtyDelivery.HasValue
                        && o.QtySaleConfirm.Value == o.QtyDelivery.Value).ToList();
                }
                else if (s.ShipmentStatus == ShipmentStatus.NotComplete)
                {
                    rs = rs.Where(o => o.QtyBackOrder.HasValue && o.QtyBackOrder.Value != 0).ToList();
                }

                return rs;
            }
        }
        public List<AdvanSale> SelectAdvanceSalesForList(SearchAdvanceSalesCriteria s)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                 var rs = ctx.SpAdvanceSalesSelectForList(s.SoldTo, s.ShipTo, s.SaleConfirmation,s.SaleOrder,s.InvoiceDate,s.InvoiceNo,s.DeliveryOrder,s.DeliveryDate,s.ShipmentNo,s.ShipmentDate,s.ProductGrade,s.PO,s.DocumentNo).ToList();

                // rs = rs.Where(o => o.QtyBackOrder.HasValue && o.QtyBackOrder.Value != 0).ToList();

               // return null;
                return rs;
            }
        }
        public List<SpDashboard_Result> DashboardValue(string CustomerNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                var rs = ctx.SpDashboard(CustomerNo).ToList();

                // rs = rs.Where(o => o.QtyBackOrder.HasValue && o.QtyBackOrder.Value != 0).ToList();


                return rs;
            }
        }
        public List<SpDashboardDelivery_Result> DashboardDeliveryValue(string CustomerNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                var rs = ctx.SpDashboardDelivery(CustomerNo).ToList();

                // rs = rs.Where(o => o.QtyBackOrder.HasValue && o.QtyBackOrder.Value != 0).ToList();


                return rs;
            }
        }
        public List<SpDashboardReport1_Result> DashboardReport1(string CustomerNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                var rs = ctx.SpDashboardReport1(CustomerNo).ToList();

                // rs = rs.Where(o => o.QtyBackOrder.HasValue && o.QtyBackOrder.Value != 0).ToList();


                return rs;
            }
        }
        public List<SpDashboardReport1Quarter_Result1> DashboardReport1Quarter(string CustomerNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                var rs = ctx.SpDashboardReport1Quarter(CustomerNo).ToList();

               
                

                return rs;
            }
        }
        
        public List<SpAnnounce_Result> Announce()
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                var rs = ctx.SpAnnounce().ToList();

                // rs = rs.Where(o => o.QtyBackOrder.HasValue && o.QtyBackOrder.Value != 0).ToList();


                return rs;
            }
        }
        #endregion


        #region Constants

        //public List<Constant> SelectConstantsForList()
        //{
        //    using (SmoEntities ctx = new SmoEntities())
        //    {
        //        return ctx.SpConstantsSelectForList().ToList();
        //    }
        //}

        public Constant SelectConstantByPK(string key)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpConstantSelectByPK(key).FirstOrDefault();
            }
        }

        #endregion Constants

        #region Errors

        public string SelectErrorMsgByPK(string errorCode)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpErrorSelectByPK(errorCode).FirstOrDefault();
            }
        }

        #endregion Errors

        #region Autonumbering
        public string GetNoAutonumbering()
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpAutonumberingGetNo().FirstOrDefault();
            }
        }

        #endregion Autonumbering

        #region Orders
        public Order CreateOrder(Order order)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Orders.AddObject(order);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, order);
            }
            return order;
        }
        public Order UpdateOrder(Order order)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Orders.Attach(order);
                ctx.ObjectStateManager.GetObjectStateEntry(order).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, order);
            }
            return order;
        }
        public Order DeleteOrder(Order order)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Orders.Attach(order);
                ctx.Orders.DeleteObject(order);
                ctx.SaveChanges();
            }
            return order;
        }
        public Order SelectOrderByPK(string orderId)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpOrderSelectByPK(orderId).FirstOrDefault();
            }
        }
        public List<Order> SelectOrdersForList(string customerName, string customerNoFrom, string customerNoTo,
            DateTime? orderDateFrom, DateTime? orderDateTo, string saleConfirmNo, string paymentTermCode,
            string productNo, string pONo, string orderNo, string orderStatusCode)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpOrdersSelectForList(customerName, customerNoFrom, customerNoTo, orderDateFrom,
                    orderDateTo, saleConfirmNo, paymentTermCode, productNo, pONo, orderNo, orderStatusCode).ToList();
            }
        }
        public List<OrderForHMC> SelectOrdersForHMC(string customerName,
            string customerNoFrom, string customerNoTo,
            DateTime? orderDateFrom, DateTime? orderDateTo,
            string saleConfirmNoFrom, string saleConfirmNoTo,
            string paymentTermCode, string productNo,
            string pONo, string orderNo, string orderStatusCode,
            string segManagerCodeFrom, string segManagerCodeTo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpOrdersSelectForHMC(customerName, customerNoFrom, customerNoTo, orderDateFrom,
                    orderDateTo, saleConfirmNoFrom, saleConfirmNoTo,
                    paymentTermCode, productNo, pONo, orderNo, orderStatusCode,
                    segManagerCodeFrom, segManagerCodeTo).ToList();
            }
        }

        public Order SelectOrderBySaleConfirmNo(string saleConfirmNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpOrderSelectBySaleConfirmNo(saleConfirmNo).FirstOrDefault();
            }
        }

        #endregion

        #region OrderItems
        public OrderItem CreateOrderItem(OrderItem orderItem)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.OrderItems.AddObject(orderItem);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, orderItem);
            }
            return orderItem;
        }
        public OrderItem UpdateOrderItem(OrderItem orderItem)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.OrderItems.Attach(orderItem);
                ctx.ObjectStateManager.GetObjectStateEntry(orderItem).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, orderItem);
            }
            return orderItem;
        }
        public OrderItem DeleteOrderItem(OrderItem orderItem)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.OrderItems.Attach(orderItem);
                ctx.OrderItems.DeleteObject(orderItem);
                ctx.SaveChanges();
            }
            return orderItem;
        }
        public OrderItem SelectOrderItemByPK(string orderNo, int itemNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpOrderItemSelectByPK(orderNo, itemNo).FirstOrDefault();
            }
        }
        public List<OrderItem> SelectOrderItemsByOrderNo(string orderNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpOrderItemsSelectByOrderNo(orderNo).ToList();
            }
        }
        public List<OrderItem> SelectOrderItemsByOrderRevisionNo(string orderNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpOrderItemsSelectByOrderRevisionNo(orderNo).ToList();
            }
        }
        public void DeleteOrderItemByOrderNo(string orderNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.SpOrderItemDeleteByOrderNo(orderNo);
            }
        }
        #endregion

        #region OrderHistory
        public OrderHistory CreateOrderHistory(OrderHistory orderHistory)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.OrderHistories.AddObject(orderHistory);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, orderHistory);
            }
            return orderHistory;
        }
        public List<OrderHistory> SelectOrderHistoryByOrderNo(string orderNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpOrderHistorySelectByOrderNo(orderNo).ToList();
            }
        }

        #endregion

        #region OrderStatus
        public OrderStatus CreateOrderStatus(OrderStatus orderStatus)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.OrderStatus.AddObject(orderStatus);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, orderStatus);
            }
            return orderStatus;
        }
        public OrderStatus UpdateOrderStatus(OrderStatus orderStatus)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.OrderStatus.Attach(orderStatus);
                ctx.ObjectStateManager.GetObjectStateEntry(orderStatus).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, orderStatus);
            }
            return orderStatus;
        }
        public OrderStatus DeleteOrderStatus(OrderStatus orderStatus)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.OrderStatus.Attach(orderStatus);
                ctx.OrderStatus.DeleteObject(orderStatus);
                ctx.SaveChanges();
            }
            return orderStatus;
        }
        public OrderStatus SelectOrderStatusByPK(string orderStatusCode)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpOrderStatusSelectByPK(orderStatusCode).FirstOrDefault();
            }
        }
        public List<OrderStatus> SelectOrderStatusForList()
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpOrderStatusSelectForList().ToList();
            }
        }

        #endregion

        #region Users
        public User CreateUser(User user)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Users.AddObject(user);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, user);
            }
            return user;
        }
        public User UpdateUser(User user)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Users.Attach(user);
                ctx.ObjectStateManager.GetObjectStateEntry(user).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, user);
            }
            return user;
        }
        //public User DeleteUser(User user)
        //{
        //    using (SmoEntities ctx = new SmoEntities())
        //    {
        //        ctx.Users.Attach(user);
        //        ctx.Users.DeleteObject(user);
        //        ctx.SaveChanges();
        //    }
        //    return user;
        //}
        //public User SelectUserByPK(string customerNo, string username)
        //{
        //    using (SmoEntities ctx = new SmoEntities())
        //    {
        //        return ctx.SpUserSelectByPK(customerNo, username).FirstOrDefault();
        //    }
        //}
        public User UserAuthentication(string username, string password)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpUserAuthentication(username, password).FirstOrDefault();
            }
        }
        public User SelectUserByPK(string username)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpUserSelectByPK(username).FirstOrDefault();
            }
        }
        public User SelectUserCreateByOrderNo(string orderNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpUserCreateSelectByOrderNo(orderNo).FirstOrDefault();
            }
        }
        #endregion

        #region Customers
        public Customer CreateCustomer(Customer customer)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Customers.AddObject(customer);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, customer);
            }
            return customer;
        }
        public Customer UpdateCustomer(Customer customer)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Customers.Attach(customer);
                ctx.ObjectStateManager.GetObjectStateEntry(customer).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, customer);
            }
            return customer;
        }
        public Customer DeleteCustomer(Customer customer)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Customers.Attach(customer);
                ctx.Customers.DeleteObject(customer);
                ctx.SaveChanges();
            }
            return customer;
        }
        public Customer SelectCustomerByPK(string customerNo, string saleOrg, string distCh, string division)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpCustomerSelectByPK(customerNo, saleOrg, distCh, division).FirstOrDefault();
            }
        }

        public List<Customer> SelectCustomersForList(string customerNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpCustomersSelectForList(customerNo).ToList();
            }
        }

        #endregion

        #region Materials
        public Material CreateMaterial(Material material)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Materials.AddObject(material);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, material);
            }
            return material;
        }
        public Material UpdateMaterial(Material material)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Materials.Attach(material);
                ctx.ObjectStateManager.GetObjectStateEntry(material).SetModified();
                ctx.SaveChanges();
                //ctx.Refresh(RefreshMode.StoreWins, material);
            }
            return SelectMaterialByPK(material.MaterialNo);
        }
        public Material DeleteMaterial(Material material)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Materials.Attach(material);
                ctx.Materials.DeleteObject(material);
                ctx.SaveChanges();
            }
            return material;
        }
        public Material SelectMaterialByPK(string materialNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpMaterialSelectByPK(materialNo).FirstOrDefault();
            }
        }
        public List<Material> SelectMaterialsProductNo(string productNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpMaterialsSelectByProductNo(productNo).ToList();
            }
        }
        public Material SelectMaterialByByProductNoAndPackageSizeCode(string productNo, string packageSizeCode)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpMaterialSelectByProductNoAndPackageSizeCode(productNo, packageSizeCode).FirstOrDefault();
            }
        }

        #endregion

        #region ProductFavs
        public ProductFav CreateProductFav(ProductFav productFav)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.ProductFavs.AddObject(productFav);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, productFav);
            }
            return productFav;
        }
        public ProductFav UpdateProductFav(ProductFav productFav)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.ProductFavs.Attach(productFav);
                ctx.ObjectStateManager.GetObjectStateEntry(productFav).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, productFav);
            }
            return productFav;
        }
        public ProductFav DeleteProductFav(ProductFav productFav)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.ProductFavs.Attach(productFav);
                ctx.ProductFavs.DeleteObject(productFav);
                ctx.SaveChanges();
            }
            return productFav;
        }
        public ProductFav SelectProductFavByPK(string customerNo, string productNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpProductFavSelectByPK(customerNo, productNo).FirstOrDefault();
            }
        }

        #endregion

        #region ProductMaterialMaps
        public ProductMaterialMap CreateProductMaterialMap(ProductMaterialMap productMaterialMap)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.ProductMaterialMaps.AddObject(productMaterialMap);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, productMaterialMap);
            }
            return productMaterialMap;
        }
        public ProductMaterialMap UpdateProductMaterialMap(ProductMaterialMap productMaterialMap)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.ProductMaterialMaps.Attach(productMaterialMap);
                ctx.ObjectStateManager.GetObjectStateEntry(productMaterialMap).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, productMaterialMap);
            }
            return productMaterialMap;
        }
        public ProductMaterialMap DeleteProductMaterialMap(ProductMaterialMap productMaterialMap)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.ProductMaterialMaps.Attach(productMaterialMap);
                ctx.ProductMaterialMaps.DeleteObject(productMaterialMap);
                ctx.SaveChanges();
            }
            return productMaterialMap;
        }
        public ProductMaterialMap SelectProductMaterialMapByPK(string productNo, string packageSizeCode)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpProductMaterialMapSelectByPK(productNo, packageSizeCode).FirstOrDefault();
            }
        }

        #endregion

        #region Products
        public Product CreateProduct(Product product)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Products.AddObject(product);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, product);
            }
            return product;
        }
        public Product UpdateProduct(Product product)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Products.Attach(product);
                ctx.ObjectStateManager.GetObjectStateEntry(product).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, product);
            }
            return product;
        }
        public Product DeleteProduct(Product product)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Products.Attach(product);
                ctx.Products.DeleteObject(product);
                ctx.SaveChanges();
            }
            return product;
        }
        public Product SelectProductByPK(string productNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpProductSelectByPK(productNo).FirstOrDefault();
            }
        }
        public List<Product> SelectProductByCustomerNo(string customerNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpProductsSelectByCustomerNo(customerNo).ToList();
            }
        }
        public List<Product> SelectProductExceptCustomerNo(string customerNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpProductsSelectExceptCustomerNo(customerNo).ToList();
            }
        }
        #endregion

        #region PackageSizes
        public PackageSize CreatePackageSize(PackageSize packageSize)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.PackageSizes.AddObject(packageSize);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, packageSize);
            }
            return packageSize;
        }
        public PackageSize UpdatePackageSize(PackageSize packageSize)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.PackageSizes.Attach(packageSize);
                ctx.ObjectStateManager.GetObjectStateEntry(packageSize).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, packageSize);
            }
            return packageSize;
        }
        public PackageSize DeletePackageSize(PackageSize packageSize)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.PackageSizes.Attach(packageSize);
                ctx.PackageSizes.DeleteObject(packageSize);
                ctx.SaveChanges();
            }
            return packageSize;
        }
        public PackageSize SelectPackageSizeByPK(string packageSizeCode)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpPackageSizeSelectByPK(packageSizeCode).FirstOrDefault();
            }
        }
        public List<PackageSize> SelectPackageSizesByProductNo(string productNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpPackageSizesSelectByProductNo(productNo).ToList();
            }
        }


        public List<PackageSize> SelectPackageSizesByProductNoWithCustomer(string productNo, string customerNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpPackageSizesSelectByProductNoWithCustomer(productNo, customerNo).ToList();
            }
        }


        #endregion

        #region PaymentTerms
        public PaymentTerm CreatePaymentTerm(PaymentTerm paymentTerm)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.PaymentTerms.AddObject(paymentTerm);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, paymentTerm);
            }
            return paymentTerm;
        }
        public PaymentTerm UpdatePaymentTerm(PaymentTerm paymentTerm)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.PaymentTerms.Attach(paymentTerm);
                ctx.ObjectStateManager.GetObjectStateEntry(paymentTerm).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, paymentTerm);
            }
            return paymentTerm;
        }
        public PaymentTerm DeletePaymentTerm(PaymentTerm paymentTerm)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.PaymentTerms.Attach(paymentTerm);
                ctx.PaymentTerms.DeleteObject(paymentTerm);
                ctx.SaveChanges();
            }
            return paymentTerm;
        }
        public PaymentTerm SelectPaymentTermByPK(string paymentTermCode)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpPaymentTermSelectByPK(paymentTermCode).FirstOrDefault();
            }
        }
        public List<PaymentTerm> SelectPaymentTermByCustomerNo(string customerNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpPaymentTermsSelectByCustomerNo(customerNo).ToList();
            }
        }
        #endregion

        #region CustomerShipToMaps
        public CustomerShipToMap CreateCustomerShipToMap(CustomerShipToMap customerShipToMap)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.CustomerShipToMaps.AddObject(customerShipToMap);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, customerShipToMap);
            }
            return customerShipToMap;
        }
        public CustomerShipToMap UpdateCustomerShipToMap(CustomerShipToMap customerShipToMap)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.CustomerShipToMaps.Attach(customerShipToMap);
                ctx.ObjectStateManager.GetObjectStateEntry(customerShipToMap).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, customerShipToMap);
            }
            return customerShipToMap;
        }
        public CustomerShipToMap DeleteCustomerShipToMap(CustomerShipToMap customerShipToMap)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.CustomerShipToMaps.Attach(customerShipToMap);
                ctx.CustomerShipToMaps.DeleteObject(customerShipToMap);
                ctx.SaveChanges();
            }
            return customerShipToMap;
        }
        public CustomerShipToMap SelectCustomerShipToMapByPK(string customerNo, string saleOrg, string distCh, string division, string shipToCode)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpCustomerShipToMapSelectByPK(customerNo, saleOrg, distCh, division, shipToCode).FirstOrDefault();
            }
        }
        public List<CustomerShipToMap> SelectCustomerShipToMapsByCustomerNo(string customerNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpCustomerShipToMapsSelectByCustomerNo(customerNo).ToList();
            }
        }

        #endregion

        #region CustomerGroups
        public CustomerGroup CreateCustomerGroup(CustomerGroup customerGroup)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.CustomerGroups.AddObject(customerGroup);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, customerGroup);
            }
            return customerGroup;
        }
        public CustomerGroup UpdateCustomerGroup(CustomerGroup customerGroup)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.CustomerGroups.Attach(customerGroup);
                ctx.ObjectStateManager.GetObjectStateEntry(customerGroup).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, customerGroup);
            }
            return customerGroup;
        }
        public CustomerGroup DeleteCustomerGroup(CustomerGroup customerGroup)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.CustomerGroups.Attach(customerGroup);
                ctx.CustomerGroups.DeleteObject(customerGroup);
                ctx.SaveChanges();
            }
            return customerGroup;
        }
        public CustomerGroup SelectCustomerGroupByPK(string customerGroupCode)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpCustomerGroupSelectByPK(customerGroupCode).FirstOrDefault();
            }
        }
        public List<CustomerGroup> SelectCustomerGroupsForList()
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpCustomerGroupsSelectForList().ToList();
            }
        }

        #endregion

        #region CustomerPaymentTermMaps
        public CustomerPaymentTermMap CreateCustomerPaymentTermMap(CustomerPaymentTermMap customerPaymentTermMap)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.CustomerPaymentTermMaps.AddObject(customerPaymentTermMap);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, customerPaymentTermMap);
            }
            return customerPaymentTermMap;
        }
        public CustomerPaymentTermMap UpdateCustomerPaymentTermMap(CustomerPaymentTermMap customerPaymentTermMap)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.CustomerPaymentTermMaps.Attach(customerPaymentTermMap);
                ctx.ObjectStateManager.GetObjectStateEntry(customerPaymentTermMap).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, customerPaymentTermMap);
            }
            return customerPaymentTermMap;
        }
        public CustomerPaymentTermMap DeleteCustomerPaymentTermMap(CustomerPaymentTermMap customerPaymentTermMap)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.CustomerPaymentTermMaps.Attach(customerPaymentTermMap);
                ctx.CustomerPaymentTermMaps.DeleteObject(customerPaymentTermMap);
                ctx.SaveChanges();
            }
            return customerPaymentTermMap;
        }
        public CustomerPaymentTermMap SelectCustomerPaymentTermMapByPK(string customerNo, string paymentTermCode)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpCustomerPaymentTermMapSelectByPK(customerNo, paymentTermCode).FirstOrDefault();
            }
        }
        public List<CustomerPaymentTermMap> SelectCustomerPaymentTermMapsForList(string customerNo, string paymentTermCode)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpCustomerPaymentTermMapsSelectForList(customerNo, paymentTermCode).ToList();
            }
        }

        #endregion

        #region SegmentManagers
        public SegmentManager CreateSegmentManager(SegmentManager segmentManager)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.SegmentManagers.AddObject(segmentManager);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, segmentManager);
            }
            return segmentManager;
        }
        public SegmentManager UpdateSegmentManager(SegmentManager segmentManager)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.SegmentManagers.Attach(segmentManager);
                ctx.ObjectStateManager.GetObjectStateEntry(segmentManager).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, segmentManager);
            }
            return segmentManager;
        }
        public SegmentManager DeleteSegmentManager(SegmentManager segmentManager)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.SegmentManagers.Attach(segmentManager);
                ctx.SegmentManagers.DeleteObject(segmentManager);
                ctx.SaveChanges();
            }
            return segmentManager;
        }
        public SegmentManager SelectSegmentManagerByPK(string segmentManagerCode)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpSegmentManagerSelectByPK(segmentManagerCode).FirstOrDefault();
            }
        }
        public List<SegmentManager> SelectSegmentManagersForList()
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpSegmentManagersSelectForList().ToList();
            }
        }

        #endregion


        #region SendMails
        public SendMail CreateSendMail(SendMail sendMail)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.SendMails.AddObject(sendMail);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, sendMail);
            }
            return sendMail;
        }
        public SendMail UpdateSendMail(SendMail sendMail)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.SendMails.Attach(sendMail);
                ctx.ObjectStateManager.GetObjectStateEntry(sendMail).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, sendMail);
            }
            return sendMail;
        }
        public SendMail DeleteSendMail(SendMail sendMail)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.SendMails.Attach(sendMail);
                ctx.SendMails.DeleteObject(sendMail);
                ctx.SaveChanges();
            }
            return sendMail;
        }
        public SendMail SelectSendMailByPK(string sendMailId)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpSendMailSelectByPK(sendMailId).FirstOrDefault();
            }
        }
        public List<SendMail> SelectSendMailsForList(bool isSent)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpSendMailsSelectForList(isSent).ToList();
            }
        }

        #endregion




        #region MailPDFs
        public MailPDF CreateMailPDF(MailPDF mailPDF)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.MailPDFs.AddObject(mailPDF);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, mailPDF);
            }
            return mailPDF;
        }
        public MailPDF DeleteMailPDF(MailPDF mailPDF)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.MailPDFs.Attach(mailPDF);
                ctx.MailPDFs.DeleteObject(mailPDF);
                ctx.SaveChanges();
            }
            return mailPDF;
        }
        public MailPDF SelectMailPDFByPK(string saleConfirmNo)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpMailPDFSelectByPK(saleConfirmNo).FirstOrDefault();
            }
        }
        public List<MailPDFSelectForList> SelectMailPDFsSelectForList(string invoice)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpMailPDFsSelectForList(invoice).ToList();
            }
        }

        #endregion


        #region Attachments
        public Attachment CreateAttachment(Attachment attachment)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Attachments.AddObject(attachment);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, attachment);
            }
            return attachment;
        }
        public Attachment UpdateAttachment(Attachment attachment)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Attachments.Attach(attachment);
                ctx.ObjectStateManager.GetObjectStateEntry(attachment).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, attachment);
            }
            return attachment;
        }
        public Attachment DeleteAttachment(Attachment attachment)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.Attachments.Attach(attachment);
                ctx.Attachments.DeleteObject(attachment);
                ctx.SaveChanges();
            }
            return attachment;
        }
        public Attachment SelectAttachmentByPK(string attachmentId)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpAttachmentSelectByPK(attachmentId).FirstOrDefault();
            }
        }
        public List<AttachmentForList> SelectAttachmentsForList(string orderNo, string note)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpAttachmentsSelectForList(orderNo, note).ToList();
            }
        }
        public List<Attachment> SelectAttachmentsByNote(string note)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpAttachmentsSelectByNote(note).ToList();
            }
        }
        #endregion



        public decimal SelectTaxPercentByTaxClass(string taxClass)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpTaxPercentSelectByTaxClass(taxClass).FirstOrDefault().Value;
            }
        }


        #region ReportShipmentDetails
        public ReportShipmentDetail CreateReportShipmentDetail(ReportShipmentDetail reportShipmentDetail)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.ReportShipmentDetails.AddObject(reportShipmentDetail);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, reportShipmentDetail);
            }
            return reportShipmentDetail;
        }
        public ReportShipmentDetail UpdateReportShipmentDetail(ReportShipmentDetail reportShipmentDetail)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.ReportShipmentDetails.Attach(reportShipmentDetail);
                ctx.ObjectStateManager.GetObjectStateEntry(reportShipmentDetail).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, reportShipmentDetail);
            }
            return reportShipmentDetail;
        }
        public ReportShipmentDetail DeleteReportShipmentDetail(ReportShipmentDetail reportShipmentDetail)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.ReportShipmentDetails.Attach(reportShipmentDetail);
                ctx.ReportShipmentDetails.DeleteObject(reportShipmentDetail);
                ctx.SaveChanges();
            }
            return reportShipmentDetail;
        }
        public void DeleteReportShipmentDetails(string  guid)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.SpReportShipmentDetailsDelete(guid);
            }
         //   return reportShipmentDetail;
        }



        public ReportShipmentDetail SelectReportShipmentDetailByPK(string reportShipmentDetailID)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpReportShipmentDetailSelectByPK(reportShipmentDetailID).FirstOrDefault();
            }
        }

        public List<ReportShipmentDetailForList> SelectReportShipmentDetailsForList(SearchShipmentCriteria s)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpReportShipmentDetailsSelectForList(s.ShipmentNo, s.DeliveryNo, s.SaleOrder, s.QuotationNo, s.SoldTo, s.ShipTo, s.DeliveryDate, s.ProductName
                    , s.DocNo, s.InvoiceNo, s.CustRef, s.SMONo,s.InvoiceType).ToList();
            }
        }

        //private string E2N(string str)
        //{
        //    var newStr = string.IsNullOrEmpty(str) ? null : str);
        //    return newStr;
        //}

        #endregion



        #region AdvanSale
        public AdvanSale CreateAdvanSale(AdvanSale advanSale)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.AdvanSales.AddObject(advanSale);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, advanSale);
            }
            return advanSale;
        }
        public AdvanSale UpdateAdvanSale(AdvanSale advanSale)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.AdvanSales.Attach(advanSale);
                ctx.ObjectStateManager.GetObjectStateEntry(advanSale).SetModified();
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, advanSale);
            }
            return advanSale;
        }
        public PaymentTerm SelectAdvanSaleByPK(string paymentTermCode)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpPaymentTermSelectByPK(paymentTermCode).FirstOrDefault();
            }
        }

        public void DeleteAdvanSaleByPK(string InvoiceDate, string ShiptmentNo, string Delivery)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.SpAdvanSaleDeleteByPK(InvoiceDate, ShiptmentNo, Delivery);
            //    return null;
            //   return ctx.SpMaterialSelectByPK(materialNo).FirstOrDefault();
            }
        }

        public List<AdvanSale> SearchAdvanSaleList()
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.AdvanSales.ToList();
            }
        }


        #endregion

        #region Announce
        public announce CreateAnnounce(announce anno)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.announces.AddObject(anno);
                ctx.SaveChanges();
                ctx.Refresh(RefreshMode.StoreWins, anno);
            }
            return anno;
        }
        public announce UpdateAnnounce(announce anno)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                ctx.SpAnnounceUpdate(anno.announce_id,anno.announce_text,anno.valid_from,anno.valid_to);
                //ctx.announces.Attach(anno);
                //ctx.ObjectStateManager.GetObjectStateEntry(anno).SetModified();
                //ctx.SaveChanges();
                //ctx.Refresh(RefreshMode.StoreWins, anno);
            }
            return anno;
        }


        public List<SpAnnounceSelectForList_Result> SelectAnnounceForList(int aid)
        {
            using (SmoEntities ctx = new SmoEntities())
            {
                return ctx.SpAnnounceSelectForList(aid).ToList();
            }
        }

        #endregion
    }
}