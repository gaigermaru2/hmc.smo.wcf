﻿<HeaderEN>
<br>
We are pleased to inform you that your order via SmartOrder no. <DocNo> has been completed as per the below details.
<br>
<br>
บริษัทฯ มีความยินดีจะแจ้งให้ท่านทราบว่าเอกสารการสั่งซื้อเลขที่ <DocNo>  ได้ถูกดำเนินการจนเสร็จสมบูรณ์แล้ว ดังมีรายละเอียดตามที่ปรากฏในตารางด้านล่างนี้
<br>
<br>
Sales Confirmation no. <SaleConfirmNo>
<br>
<br>
<LineItems>
<br>
<br>
Please review the content. If it is not correct, please reject within 1 working days, otherwise, it shall be deemed that you have accepted and agreed that the Sales Confirmation is correct and agreed to be bound by it.
<br>
<FooterEN>