﻿using System;
using System.Collections.Generic;

namespace Hmc.Smo.Wcf
{
    public class Service : IService
    {
        public WebConfig GetWebConfig()
        {
            //return new WebConfig();
            return new SmoBC().GetWebConfig();
        }
        public void SendEmailForInvoice(Customer c, List<Invoice> items, string fileName)
        {
            new SmoBC().SendEmailForInvoice(c, items, fileName);
        }
        public void SendEmailForInvoiceCN(Customer c, List<Invoice> items, string fileName)
        {
            new SmoBC().SendEmailForInvoiceCN(c, items, fileName);
        }

        #region OrderTemplates
        public OrderTemplate CreateOrderTemplate(OrderTemplate orderTemplate)
        {
            return new SmoBC().CreateOrderTemplate(orderTemplate);
        }
        public OrderTemplate UpdateOrderTemplate(OrderTemplate orderTemplate)
        {
            return new SmoBC().UpdateOrderTemplate(orderTemplate);
        }
        public OrderTemplate DeleteOrderTemplate(OrderTemplate orderTemplate)
        {
            return new SmoBC().DeleteOrderTemplate(orderTemplate);
        }
        public OrderTemplate SelectOrderTemplateByPK(string iD, string customerNo)
        {
            return new SmoBC().SelectOrderTemplateByPK(iD, customerNo);
        }
        public List<OrderTemplateForList> SelectOrderTemplatesForList(string customerNo, bool? allowDownload)
        {
            return new SmoBC().SelectOrderTemplatesForList(customerNo, allowDownload);
        }

        #endregion


        #region ReportShipmentBalances
        public ReportShipmentBalance CreateReportShipmentBalance(ReportShipmentBalance reportShipmentBalance)
        {
            return new SmoBC().CreateReportShipmentBalance(reportShipmentBalance);
        }
        public ReportShipmentBalance UpdateReportShipmentBalance(ReportShipmentBalance reportShipmentBalance)
        {
            return new SmoBC().UpdateReportShipmentBalance(reportShipmentBalance);
        }
        public ReportShipmentBalance DeleteReportShipmentBalance(ReportShipmentBalance reportShipmentBalance)
        {
            return new SmoBC().DeleteReportShipmentBalance(reportShipmentBalance);
        }
        public ReportShipmentBalance SelectReportShipmentBalanceByPK(string reportShipmentBalanceID)
        {
            return new SmoBC().SelectReportShipmentBalanceByPK(reportShipmentBalanceID);
        }
        public List<ReportShipmentBalance> SelectReportShipmentBalancesForList(SearchShipmentCriteria s)
        {
            return new SmoBC().SelectReportShipmentBalancesForList(s);
        }

        #endregion


        #region Constants

        //public List<Constant> SelectConstantsForList()
        //{
        //    SmoBC bc = new SmoBC();
        //    return bc.SelectConstantsForList();
        //}

        public Constant SelectConstantByPK(string key)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectConstantByPK(key);
        }
        #endregion Constants

        #region Errors
        public string SelectErrorMsgByPK(string errorCode)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectErrorMsgByPK(errorCode);
        }
        #endregion

        #region Orders
        public Order CreateOrder(Order order, List<OrderItem> orderItems, string tempIdForAttachment)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateOrder(order, orderItems, tempIdForAttachment);
        }
        public void MarkOrderStatusToBeCancelled(string orderStatusCode)
        {
            SmoBC bc = new SmoBC();
            bc.MarkOrderStatusToBeCancelled(orderStatusCode);
        }
        public void MarkOrderStatusToBeCompletedWithPdf(string saleConfirmNo, byte[] pdfContent)
        {
            SmoBC bc = new SmoBC();
            bc.MarkOrderStatusToBeCompletedWithPdf(saleConfirmNo, pdfContent);
        }
        public void MarkOrderStatusToBeCompleted(List<SaleConfirmStatusSMO13> saleConfirmNos)
        {
            SmoBC bc = new SmoBC();
            bc.MarkOrderStatusToBeCompleted(saleConfirmNos);
        }
        public Order UpdateOrder(Order order, List<OrderItem> orderItems)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateOrder(order, orderItems);
        }
        public Order ChangeOrderStatus(string orderNo, string changeBy, string newStatus, string reason)
        {
            SmoBC bc = new SmoBC();
            return bc.ChangeOrderStatus(orderNo, changeBy, newStatus, reason);
        }
        public Order DeleteOrder(Order order)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteOrder(order);
        }
        public Order SelectOrderByPK(string orderNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectOrderByPK(orderNo);
        }
        public List<Order> SelectOrdersForList(string customerName, string customerNoFrom, string customerNoTo,
            DateTime? orderDateFrom, DateTime? orderDateTo, string saleConfirmNo, string paymentTermCode,
            string productNo, string pONo, string orderNo, string orderStatusCode)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectOrdersForList(customerName, customerNoFrom, customerNoTo, orderDateFrom,
                    orderDateTo, saleConfirmNo, paymentTermCode, productNo, pONo, orderNo, orderStatusCode);
        }
        public List<OrderForHMC> SelectOrdersForHMC(string customerName,
            string customerNoFrom, string customerNoTo,
            DateTime? orderDateFrom, DateTime? orderDateTo,
            string saleConfirmNoFrom, string saleConfirmNoTo,
            string paymentTermCode, string productNo,
            string pONo, string orderNo, string orderStatusCode,
            string segManagerCodeFrom, string segManagerCodeTo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectOrdersForHMC(customerName, customerNoFrom, customerNoTo, orderDateFrom,
                    orderDateTo, saleConfirmNoFrom, saleConfirmNoTo, paymentTermCode, productNo, pONo, orderNo, orderStatusCode,
                    segManagerCodeFrom, segManagerCodeTo);
        }
        #endregion

        #region OrderItems
        public OrderItem CreateOrderItem(OrderItem orderItem)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateOrderItem(orderItem);
        }
        public OrderItem UpdateOrderItem(OrderItem orderItem)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateOrderItem(orderItem);
        }
        public OrderItem DeleteOrderItem(OrderItem orderItem)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteOrderItem(orderItem);
        }
        public OrderItem SelectOrderItemByPK(string orderNo, int itemNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectOrderItemByPK(orderNo, itemNo);
        }
        public List<OrderItem> SelectOrderItemsByOrderNo(string orderNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectOrderItemsByOrderNo(orderNo);
        }
        public List<OrderItem> SelectOrderItemsByOrderRevisionNo(string orderNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectOrderItemsByOrderRevisionNo(orderNo);
        }
        #endregion

        #region OrderHistory
        public OrderHistory CreateOrderHistory(OrderHistory orderHistory)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateOrderHistory(orderHistory);
        }
        public List<OrderHistory> SelectOrderHistoryByOrderNo(string orderNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectOrderHistoryByOrderNo(orderNo);
        }

        #endregion

        #region OrderStatus
        public OrderStatus CreateOrderStatus(OrderStatus orderStatus)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateOrderStatus(orderStatus);
        }
        public OrderStatus UpdateOrderStatus(OrderStatus orderStatus)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateOrderStatus(orderStatus);
        }
        public OrderStatus DeleteOrderStatus(OrderStatus orderStatus)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteOrderStatus(orderStatus);
        }
        public OrderStatus SelectOrderStatusByPK(string orderStatusCode)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectOrderStatusByPK(orderStatusCode);
        }
        public List<OrderStatus> SelectOrderStatusForList()
        {
            SmoBC bc = new SmoBC();
            return bc.SelectOrderStatusForList();
        }

        #endregion

        #region Users
        public User CreateUser(User user)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateUser(user);
        }
        public User UpdateUser(User user)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateUser(user);
        }
        //public User DeleteUser(User user)
        //{
        //    SmoBC bc = new SmoBC();
        //    return bc.DeleteUser(user);
        //}
        //public User SelectUserByPK(string customerNo, string username)
        //{
        //    SmoBC bc = new SmoBC();
        //    return bc.SelectUserByPK(customerNo, username);
        //}
        public User UserAuthentication(string username, string password)
        {
            SmoBC bc = new SmoBC();
            return bc.UserAuthentication(username, password);
        }
        public User SelectUserByPK(string customerNo, string username)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectUserByPK(customerNo, username);
        }
        public User SelectUserCreateByOrderNo(string orderNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectUserCreateByOrderNo(orderNo);
        }
        #endregion

        #region Customers
        public Customer CreateCustomer(Customer customer)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateCustomer(customer);
        }
        public Customer UpdateCustomer(Customer customer)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateCustomer(customer);
        }
        public Customer DeleteCustomer(Customer customer)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteCustomer(customer);
        }
        public Customer SelectCustomerByPK(string customerNo, string saleOrg, string distCh, string division)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectCustomerByPK(customerNo, saleOrg, distCh, division);
        }
        public List<Customer> SelectCustomersForList(string customerNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectCustomersForList(customerNo);
        }
        #endregion

        #region Materials
        public Material CreateMaterial(Material material)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateMaterial(material);
        }
        public Material UpdateMaterial(Material material)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateMaterial(material);
        }
        public Material DeleteMaterial(Material material)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteMaterial(material);
        }
        public Material SelectMaterialByPK(string materialNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectMaterialByPK(materialNo);
        }
        public List<Material> SelectMaterialsProductNo(string productNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectMaterialsProductNo(productNo);
        }
        public Material SelectMaterialByByProductNoAndPackageSizeCode(string productNo, string packageSizeCode)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectMaterialByByProductNoAndPackageSizeCode(productNo, packageSizeCode);
        }

        #endregion

        #region ProductFavs


        public ProductFav CreateProductFav(ProductFav productFav)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateProductFav(productFav);
        }
        public ProductFav UpdateProductFav(ProductFav productFav)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateProductFav(productFav);
        }
        public ProductFav DeleteProductFav(ProductFav productFav)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteProductFav(productFav);
        }
        //public ProductFav SelectProductFavByPK(string customerNo, string productNo, string packageSizeCode, string materialNo)
        //{
        //    SmoBC bc = new SmoBC();
        //    return bc.SelectProductFavByPK(customerNo, productNo, packageSizeCode, materialNo);
        //}

        #endregion

        #region ProductMaterialMaps
        public ProductMaterialMap CreateProductMaterialMap(ProductMaterialMap productMaterialMap)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateProductMaterialMap(productMaterialMap);
        }
        public ProductMaterialMap UpdateProductMaterialMap(ProductMaterialMap productMaterialMap)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateProductMaterialMap(productMaterialMap);
        }
        public ProductMaterialMap DeleteProductMaterialMap(ProductMaterialMap productMaterialMap)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteProductMaterialMap(productMaterialMap);
        }
        //public ProductMaterialMap SelectProductMaterialMapByPK(string productNo, string packageSizeCode, string materialNo)
        //{
        //    SmoBC bc = new SmoBC();
        //    return bc.SelectProductMaterialMapByPK(productNo, packageSizeCode, materialNo);
        //}

        #endregion

        #region Products
        public Product CreateProduct(Product product)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateProduct(product);
        }
        public Product UpdateProduct(Product product)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateProduct(product);
        }
        public Product DeleteProduct(Product product)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteProduct(product);
        }
        public Product SelectProductByPK(string productNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectProductByPK(productNo);
        }
        public List<Product> SelectProductByCustomerNo(string customerNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectProductByCustomerNo(customerNo);
        }
        public List<Product> SelectProductExceptCustomerNo(string customerNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectProductExceptCustomerNo(customerNo);
        }
        #endregion

        #region PackageSizes
        public PackageSize CreatePackageSize(PackageSize packageSize)
        {
            SmoBC bc = new SmoBC();
            return bc.CreatePackageSize(packageSize);
        }
        public PackageSize UpdatePackageSize(PackageSize packageSize)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdatePackageSize(packageSize);
        }
        public PackageSize DeletePackageSize(PackageSize packageSize)
        {
            SmoBC bc = new SmoBC();
            return bc.DeletePackageSize(packageSize);
        }
        public PackageSize SelectPackageSizeByPK(string packageSizeCode)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectPackageSizeByPK(packageSizeCode);
        }
        //public List<PackageSize> SelectPackageSizesForList()
        //{
        //    SmoBC bc = new SmoBC();
        //    return bc.SelectPackageSizesForList();
        //}
        public List<PackageSize> SelectPackageSizesByProductNo(string productNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectPackageSizesByProductNo(productNo);
        }
        public List<PackageSize> SelectPackageSizesByProductNoWithCustomer(string productNo, string customerNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectPackageSizesByProductNoWithCustomer(productNo, customerNo);
        }
        #endregion

        #region PaymentTerms
        public PaymentTerm CreatePaymentTerm(PaymentTerm paymentTerm)
        {
            SmoBC bc = new SmoBC();
            return bc.CreatePaymentTerm(paymentTerm);
        }
        public PaymentTerm UpdatePaymentTerm(PaymentTerm paymentTerm)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdatePaymentTerm(paymentTerm);
        }
        public PaymentTerm DeletePaymentTerm(PaymentTerm paymentTerm)
        {
            SmoBC bc = new SmoBC();
            return bc.DeletePaymentTerm(paymentTerm);
        }
        public PaymentTerm SelectPaymentTermByPK(string paymentTermCode)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectPaymentTermByPK(paymentTermCode);
        }
        public List<PaymentTerm> SelectPaymentTermByCustomerNo(string customerNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectPaymentTermByCustomerNo(customerNo);
        }
        #endregion

        #region CustomerShipToMaps

        public CustomerShipToMap CreateCustomerShipToMap(CustomerShipToMap customerShipToMap)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateCustomerShipToMap(customerShipToMap);
        }
        public CustomerShipToMap UpdateCustomerShipToMap(CustomerShipToMap customerShipToMap)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateCustomerShipToMap(customerShipToMap);
        }
        public CustomerShipToMap DeleteCustomerShipToMap(CustomerShipToMap customerShipToMap)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteCustomerShipToMap(customerShipToMap);
        }
        public CustomerShipToMap SelectCustomerShipToMapByPK(string customerNo, string saleOrg, string distCh, string division, string shipToCode)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectCustomerShipToMapByPK(customerNo, saleOrg, distCh, division, shipToCode);
        }
        public List<CustomerShipToMap> SelectCustomerShipToMapsByCustomerNo(string customerNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectCustomerShipToMapsByCustomerNo(customerNo);
        }

        #endregion

        #region CustomerGroups
        public CustomerGroup CreateCustomerGroup(CustomerGroup customerGroup)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateCustomerGroup(customerGroup);
        }
        public CustomerGroup UpdateCustomerGroup(CustomerGroup customerGroup)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateCustomerGroup(customerGroup);
        }
        public CustomerGroup DeleteCustomerGroup(CustomerGroup customerGroup)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteCustomerGroup(customerGroup);
        }
        public CustomerGroup SelectCustomerGroupByPK(string customerGroupCode)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectCustomerGroupByPK(customerGroupCode);
        }
        public List<CustomerGroup> SelectCustomerGroupsForList()
        {
            SmoBC bc = new SmoBC();
            return bc.SelectCustomerGroupsForList();
        }

        #endregion

        #region CustomerPaymentTermMaps
        public CustomerPaymentTermMap CreateCustomerPaymentTermMap(CustomerPaymentTermMap customerPaymentTermMap)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateCustomerPaymentTermMap(customerPaymentTermMap);
        }
        public CustomerPaymentTermMap UpdateCustomerPaymentTermMap(CustomerPaymentTermMap customerPaymentTermMap)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateCustomerPaymentTermMap(customerPaymentTermMap);
        }
        public CustomerPaymentTermMap DeleteCustomerPaymentTermMap(CustomerPaymentTermMap customerPaymentTermMap)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteCustomerPaymentTermMap(customerPaymentTermMap);
        }
        //public CustomerPaymentTermMap SelectCustomerPaymentTermMapByPK(string customerNo, string paymentTermCode)
        //{
        //    SmoBC bc = new SmoBC();
        //    return bc.SelectCustomerPaymentTermMapByPK(customerNo, paymentTermCode);
        //}
        public List<CustomerPaymentTermMap> SelectCustomerPaymentTermMapsForList(string customerNo, string paymentTermCode)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectCustomerPaymentTermMapsForList(customerNo, paymentTermCode);
        }

        #endregion

        #region SendEmail

        //public void SendEmailOneTo(string subject, string body, string sentTo)
        //{
        //    new SmoBC().SendEmailOneTo(subject, body, sentTo);
        //}
        //public void SendEmailOneToOneCc(string subject, string body, string sentTo, string cc)
        //{
        //    new SmoBC().SendEmailOneToOneCc(subject, body, sentTo, cc);
        //}
        //public void SendEmailOneToMultiCc(string subject, string body, string sentTo, string[] ccs)
        //{
        //    new SmoBC().SendEmailOneToMultiCc(subject, body, sentTo, ccs);
        //}

        public void SendEmail(string subject, string body, string[] sentTo, string[] ccs, string saleConfirmNo, string[] bccs)
        {
            new SmoBC().SendEmail(subject, body, sentTo, ccs, saleConfirmNo, bccs);
        }
        public void SendMail()
        {
            new SmoBC().SendMail();
        }
        public void SendEmailByCaseNo(int caseNo, User currentUser, Order order, List<OrderItem> items, string redirectUrl, byte[] pdf)
        {
            new SmoBC().SendEmailByCaseNo(caseNo, currentUser, order, items, redirectUrl, pdf);
        }

        #endregion

        #region SegmentManagers
        public SegmentManager CreateSegmentManager(SegmentManager segmentManager)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateSegmentManager(segmentManager);
        }
        public SegmentManager UpdateSegmentManager(SegmentManager segmentManager)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateSegmentManager(segmentManager);
        }
        public SegmentManager DeleteSegmentManager(SegmentManager segmentManager)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteSegmentManager(segmentManager);
        }
        //public SegmentManager SelectSegmentManagerByPK(string segmentManagerCode)
        //{
        //    SmoBC bc = new SmoBC();
        //    return bc.SelectSegmentManagerByPK(segmentManagerCode);
        //}
        public List<SegmentManager> SelectSegmentManagersForList()
        {
            SmoBC bc = new SmoBC();
            return bc.SelectSegmentManagersForList();
        }

        #endregion

        #region CreateOrModify
        public void CreateOrModifyPaymentTerms(List<PaymentTerm> paymentTerms)
        {
            SmoBC bc = new SmoBC();
            bc.CreateOrModifyPaymentTerms(paymentTerms);
        }
        public void CreateOrModifyPackageSizes(List<PackageSize> packageSizes)
        {
            SmoBC bc = new SmoBC();
            bc.CreateOrModifyPackageSizes(packageSizes);
        }
        public void CreateOrModifyCustomerGroups(List<CustomerGroup> customerGroups)
        {
            SmoBC bc = new SmoBC();
            bc.CreateOrModifyCustomerGroups(customerGroups);
        }
        public void CreateOrModifySegmentManagers(List<SegmentManager> segmentManagers)
        {
            SmoBC bc = new SmoBC();
            bc.CreateOrModifySegmentManagers(segmentManagers);
        }
        public void CreateOrModifyCustomers(List<Customer> customers)
        {
            SmoBC bc = new SmoBC();
            bc.CreateOrModifyCustomers(customers);
        }
        public void CreateOrModifyCustomerShipToMaps(List<CustomerShipToMap> customerShipToMaps)
        {
            SmoBC bc = new SmoBC();
            bc.CreateOrModifyCustomerShipToMaps(customerShipToMaps);
        }
        public void CreateOrModifyCustomerPaymentTermMaps(List<CustomerPaymentTermMap> customerPaymentTermMaps)
        {
            SmoBC bc = new SmoBC();
            bc.CreateOrModifyCustomerPaymentTermMaps(customerPaymentTermMaps);
        }
        public void CreateOrModifyMaterials(List<Material> materials)
        {
            SmoBC bc = new SmoBC();
            bc.CreateOrModifyMaterials(materials);
        }
        public void CreateOrModifyProducts(List<Product> products)
        {
            SmoBC bc = new SmoBC();
            bc.CreateOrModifyProducts(products);
        }
        public void CreateOrModifyProductMaterialMaps(List<ProductMaterialMap> productMaterialMaps)
        {
            SmoBC bc = new SmoBC();
            bc.CreateOrModifyProductMaterialMaps(productMaterialMaps);
        }
        public void CreateOrModifyProductFavs(List<ProductFav> productFavs)
        {
            SmoBC bc = new SmoBC();
            bc.CreateOrModifyProductFavs(productFavs);
        }
        public void CreateOrModifyUsers(List<User> users)
        {
            SmoBC bc = new SmoBC();
            bc.CreateOrModifyUsers(users);
        }
        public string CreateOrModifyReportShipmentDetails(List<ReportShipmentDetail> reportShipmentDetail, bool? isSendMail)
        {
            return new SmoBC().CreateOrModifyReportShipmentDetails(reportShipmentDetail, isSendMail);
        }
        public void CreateOrModifyReportShipmentBalances(List<ReportShipmentBalance> reportShipmentBalance)
        {
            new SmoBC().CreateOrModifyReportShipmentBalances(reportShipmentBalance);
        }
        public void CreateOrModifyMailPDF(MailPDF mailPDF)
        {
            new SmoBC().CreateOrModifyMailPDF(mailPDF);
        }
        #endregion

        #region SAP

        //public void BAPI_Test()
        //{
        //    SapConnection bapi = new SapConnection("ZSMO_QUO_STATUS");
        //    bapi.ExcFunction();
        //}

        public RS_ZIDOC_INPUT_QUOTATION BAPI_ZIDOC_INPUT_QUOTATION_TEST()
        {
            Customer c = new Customer()
            {
                QuotType = "ZQT",
                SaleOrg = "TH01",
                DistCh = "01",
                Division = "01",
                SaleOffice = "1234",
                CustomerNo = "999999"
            };

            Order hd = new Order()
            {
                ShipToCode = "000000",
                PONo = "PO0001",
                PODate = DateTime.Now,
                ValidityDate = DateTime.Now,
                PricingDate = DateTime.Now,
                PaymentTermCode = "T000",
                OrderNo = "15-0000003T",
                CustomerGroupCode = "01"
            };

            List<OrderItem> its = new List<OrderItem>();
            its.Add(new OrderItem()
            {
                MaterialNo = "610003.0025",
                Quantity = 5,
                UnitPrice = 10000,
                ItemNo = 1
            });

            RS_ZIDOC_INPUT_QUOTATION val = new RS_ZIDOC_INPUT_QUOTATION();
            val.OrderItems = its;
            val.Order = hd;
            val.Customer = c;
            val.User = new User() { FullName = "TEERAPONG SUNCHUMSRI" };

            return BAPI_ZIDOC_INPUT_QUOTATION(val);
        }

        public RS_ZIDOC_INPUT_QUOTATION BAPI_ZIDOC_INPUT_QUOTATION(RS_ZIDOC_INPUT_QUOTATION val)
        {
            return new SmoBC().BAPI_ZIDOC_INPUT_QUOTATION(val);
        }

        #endregion

        #region SendMails
        public SendMail CreateSendMail(SendMail sendMail)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateSendMail(sendMail);
        }
        public SendMail UpdateSendMail(SendMail sendMail)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateSendMail(sendMail);
        }
        public SendMail DeleteSendMail(SendMail sendMail)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteSendMail(sendMail);
        }
        public SendMail SelectSendMailByPK(string sendMailId)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectSendMailByPK(sendMailId);
        }
        public List<SendMail> SelectSendMailsForList(bool isSent)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectSendMailsForList(isSent);
        }

        #endregion

        #region MailPDFs
        public MailPDF CreateMailPDF(MailPDF mailPDF)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateMailPDF(mailPDF);
        }
        public MailPDF DeleteMailPDF(MailPDF mailPDF)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteMailPDF(mailPDF);
        }
        public MailPDF SelectMailPDFByPK(string saleConfirmNo)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectMailPDFByPK(saleConfirmNo);
        }
        public List<MailPDFSelectForList> SelectMailPDFsSelectForList(string invoice)
        {
            return new SmoBC().SelectMailPDFsSelectForList(invoice);
        }

        #endregion

        #region Attachments
        public Attachment CreateAttachment(Attachment attachment)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateAttachment(attachment);
        }
        public Attachment UpdateAttachment(Attachment attachment)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateAttachment(attachment);
        }
        public Attachment DeleteAttachment(Attachment attachment)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteAttachment(attachment);
        }
        public Attachment DeleteAttachmentByPK(string attachmentId)
        {
            SmoBC bc = new SmoBC();
            return bc.DeleteAttachmentByPK(attachmentId);
        }
        public Attachment SelectAttachmentByPK(string attachmentId)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectAttachmentByPK(attachmentId);
        }
        public List<AttachmentForList> SelectAttachmentsForList(string orderNo, string note)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectAttachmentsForList(orderNo, note);
        }

        #endregion

        public void _Test()
        {
            new SmoBC()._Test();
        }

        public decimal SelectTaxPercentByTaxClass(string taxClass)
        {
            return new SmoBC().SelectTaxPercentByTaxClass(taxClass);
        }

        #region ReportShipmentDetails
        public ReportShipmentDetail CreateReportShipmentDetail(ReportShipmentDetail reportShipmentDetail)
        {
            return new SmoBC().CreateReportShipmentDetail(reportShipmentDetail);
        }
        public ReportShipmentDetail UpdateReportShipmentDetail(ReportShipmentDetail reportShipmentDetail)
        {
            return new SmoBC().UpdateReportShipmentDetail(reportShipmentDetail);
        }
        public ReportShipmentDetail DeleteReportShipmentDetail(ReportShipmentDetail reportShipmentDetail)
        {
            return new SmoBC().DeleteReportShipmentDetail(reportShipmentDetail);
        }
        public ReportShipmentDetail SelectReportShipmentDetailByPK(string reportShipmentDetailID)
        {
            return new SmoBC().SelectReportShipmentDetailByPK(reportShipmentDetailID);
        }
        public List<ReportShipmentDetailForList> SelectReportShipmentDetailsForList(SearchShipmentCriteria s)
        {
            return new SmoBC().SelectReportShipmentDetailsForList(s);
        }



        //public List<ReportShipmentDetail> LoadTest(SearchShipmentDetail search)
        //{
        //    throw new NotImplementedException();
        //}

        #endregion


        #region CreateOrModify
        public void CreateAdvanSale(List<AdvanSale> ads)
        {
            SmoBC bc = new SmoBC();
            bc.CreateAdvanSale(ads);
            //bc.CreateOrModifyPaymentTerms(paymentTerms);
        }
        public void DeleteAdvanSaleByPK(string InvoiceDate, string ShiptmentNo, string Delivery)
        {
            SmoBC bc = new SmoBC();
            //bc.CreateAdvanSale(ads);
            bc.DeleteAdvanSaleByPK(InvoiceDate, ShiptmentNo, Delivery);
            //bc.CreateOrModifyPaymentTerms(paymentTerms);
        }

        public List<AdvanSale> SearchAdvanSale()
        {
            SmoBC bc = new SmoBC();
            //bc.CreateAdvanSale(ads);
           return bc.SearchAdvanSale();
            //bc.CreateOrModifyPaymentTerms(paymentTerms);
        }

        //public List<AdvanSale> SearchAdvanSaleForList()
        //{
        //    SmoBC bc = new SmoBC();
        //    //bc.CreateAdvanSale(ads);
        //    return bc.SearchAdvanSale();
        //    //bc.CreateOrModifyPaymentTerms(paymentTerms);
        //}
        public List<AdvanSale> SearchAdvanSaleForList(SearchAdvanceSalesCriteria s)
        {
            return new SmoBC().SelectAdvanceSalesForList(s);
        }
        public List<SpDashboard_Result> DashboardValue(string CustomerNo)
        {
            return new SmoBC().DashboardValue(CustomerNo);
        }
        public List<SpDashboardDelivery_Result> DashboardDeliveryValue(string CustomerNo)
        {
            return new SmoBC().DashboardDeliveryValue(CustomerNo);
        }
        public List<SpAnnounce_Result> Announce()
        {
            return new SmoBC().Announce();
        }
        public List<SpDashboardReport1_Result> DashboardReport1(string CustomerNo)
        {
            return new SmoBC().DashboardReport1(CustomerNo);
        }
        public List<SpDashboardReport1Quarter_Result1> DashboardReport1Quarter(string CustomerNo)
        {
            return new SmoBC().DashboardReport1Quarter(CustomerNo);
        }
        #endregion


        #region Announce
        public announce CreateAnnounce(announce anno)
        {
            SmoBC bc = new SmoBC();
            return bc.CreateAnnounce(anno);
        }
        public announce UpdateAnnounce(announce anno)
        {
            SmoBC bc = new SmoBC();
            return bc.UpdateAnnounce(anno);
        }

        public List<SpAnnounceSelectForList_Result> SelectAnnounceForList(int aid)
        {
            SmoBC bc = new SmoBC();
            return bc.SelectAnnounceForList(aid);
        }
        #endregion
    }
}
